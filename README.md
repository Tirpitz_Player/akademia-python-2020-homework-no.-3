# Weapon level checking module [![pipeline status](https://gitlab.com/Tirpitz_Player/akademia-python-2020-homework-no.-3/badges/master/pipeline.svg)](https://gitlab.com/Tirpitz_Player/akademia-python-2020-homework-no.-3/-/commits/master) [![coverage report](https://gitlab.com/Tirpitz_Player/akademia-python-2020-homework-no.-3/badges/master/coverage.svg)](https://gitlab.com/Tirpitz_Player/akademia-python-2020-homework-no.-3/-/commits/master)

This is homework project for Akademia Python 2019/2020

## Table of contents
* Technologies
* Setup
* Author of project

## Technologies
Project is created with:
* Python 3

## Author of project
@Tirpitz_Player aka Mikołaj Błażejewski
