#!/usr/bin/enc python3
""" Checking if the player has enough level to be able to put on a given piece of equipment """

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT License"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""


# Rest of code
class EquipmentLvlRequirements:
    def __init__(self, req_lvl: int, player_lvl: int):
        """
        Human Class
        :param req_lvl: Required level for equipment fitting
        :param player_lvl: Player level
        """
        self._req_lvl = req_lvl  # If is "_" after "." then this variable is protected
        self._player_lvl = player_lvl  # If is "_" after "." then this variable is protected

    @property  # Thanks to this, the class will behave like a variable
    def req_lvl(self) -> int:
        """
        Returning requirement eq level
        :return int: Returning requirement eq level
        """
        return self._req_lvl

    @property  # Thanks to this, the class will behave like a variable
    def player_lvl(self) -> int:
        """
        Returning player level
        :return int: Returning player level
        """
        return self._player_lvl

    @property
    def fulfills(self) -> bool:
        """
        Returning information on whether a player meets the conditions
        :return bool: Returning information on whether a player meets the conditions
        """
        return self._req_lvl <= self._player_lvl
