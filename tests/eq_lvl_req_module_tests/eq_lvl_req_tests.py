#!/usr/bin/enc python3
""" Testing module called eq_lvl_req_module """

# Import from other files here
from eq_lvl_req_module.eq_lvl_req import EquipmentLvlRequirements

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT License"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""

# Import of libraries
import unittest


# Rest of code
class EquipmentLvlRequirementsTest(unittest.TestCase):
    def test_item_sword(self):
        sword = EquipmentLvlRequirements(5, 6)
        self.assertEqual(5, sword.req_lvl, "There is requirement level other than expected")
        self.assertEqual(6, sword.player_lvl, "There is player level other than expected")
        self.assertEqual(True, sword.fulfills, "The result of comparing levels is other than expected")

    def test_item_helmet(self):
        helmet = EquipmentLvlRequirements(10, 2)
        self.assertEqual(10, helmet.req_lvl, "There is requirement level other than expected")
        self.assertEqual(2, helmet.player_lvl, "There is player level other than expected")
        self.assertEqual(False, helmet.fulfills, "The result of comparing levels is other than expected")

    def test_item_armor(self):
        armor = EquipmentLvlRequirements(3, 3)
        self.assertEqual(3, armor.req_lvl, "There is requirement level other than expected")
        self.assertEqual(3, armor.player_lvl, "There is player level other than expected")
        self.assertEqual(True, armor.fulfills, "The result of comparing levels is other than expected")


if __name__ == "__main__":
    unittest.main()
