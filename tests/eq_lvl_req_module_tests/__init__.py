#!/usr/bin/enc python3
""" Init tests modules """

# Import from other files here
from tests.eq_lvl_req_module_tests.eq_lvl_req_tests import EquipmentLvlRequirementsTest

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT License"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""

# Import of libraries
import unittest

# Rest of code
if __name__ == "__main__":
    unittest.main()
