#!/usr/bin/enc python3
""" Main file """

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT License"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""

# Import of libraries
import setuptools

# Get values from README.md file
with open("README.md", "r") as fh:
    long_description = fh.read()

# Setting up information about module
setuptools.setup(
    name="eq_lvl_req_module_by_tirpitz",
    version="0.0.1",
    author="Mikołaj Błażejewski",
    author_email="",
    description="Module for checking player meets the level of weapon requirements",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Tirpitz_Player/akademia-python-2020-homework-no.-3",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)